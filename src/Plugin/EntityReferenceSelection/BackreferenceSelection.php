<?php

namespace Drupal\entity_computed_reference\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Annotation\EntityReferenceSelection;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Back Reference Selection.
 *
 * @todo Create backreference for new entities.
 *   - Add suitable prepopulate method and use it in ComputedItem.
 *     (For autocomplete refs, this is done in ::createNewEntity,
 *     but it should be more generic.)
 * @todo Add option to remove backreference for deleted entities.
 *   - Implement hook_entity_delete.
 * @todo Validate backreference cardinality for create and delete.
 *   - For create, this is easy in an entity validator.
 *   - For delete, there is no validate, we only have access.
 *
 * @EntityReferenceSelection(
 *   id = "entity_computed_reference_backreference",
 *   label = @Translation("Entity Backreference"),
 *   group = "entity_computed_reference_backreference",
 *   weight = 0,
 * )
 */
class BackreferenceSelection extends DefaultSelection {

  /**
   * @var mixed
   */
  protected $fieldTypePluginManager;

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->fieldTypePluginManager = $container->get('plugin.manager.field.field_type');
    return $instance;
  }

  /**
   * @inheritDoc
   */
  public function defaultConfiguration() {
    $configuration = parent::defaultConfiguration() + [
        'target_field_name' => NULL,
      ];
    unset($configuration['target_bundles']);
    return $configuration;
  }

  /**
   * @inheritDoc
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $formObject = $form_state->getFormObject();
    assert($formObject instanceof EntityFormInterface);
    $fieldConfig = $formObject->getEntity();
    $configuration = $this->getConfiguration();
    $targetFieldName = $configuration['target_field_name'];

    $form['target_field_name'] = [
      '#type' => 'select',
      '#title' => t('Referencing field'),
      '#options' => $this->referencingFieldOptions($fieldConfig),
      '#default_value' => $targetFieldName,
      '#required' => TRUE,
    ];
    // Hackily circumvent notices in parent method.
    $this->configuration['target_bundles'] = NULL;
    $form = parent::buildConfigurationForm($form, $form_state);
    unset($this->configuration['target_bundles']);
    unset($form['target_bundles']);
    unset($form['target_bundles_update']);
    return $form;
  }

  private function referencingFieldOptions(FieldConfigInterface $ourFieldConfig): array {
    $ourTargetType = $ourFieldConfig->getFieldStorageDefinition()->getSetting('target_type');
    $ourEntityTypeId = $ourFieldConfig->getTargetEntityTypeId();
    $ourBundle = $ourFieldConfig->getTargetBundle();
    // Getting field map is the simplest method to get all fields by bundle, see
    // comments in that method.
    $fieldsMap = $this->entityFieldManager->getFieldMap()[$ourTargetType] ?? [];
    $fieldStorageDefinitions = $this->entityFieldManager->getFieldStorageDefinitions($ourTargetType);
    $options = [];
    foreach ($fieldsMap as $fieldName => $fieldInfo) {
      $fieldStorageDefinition = $fieldStorageDefinitions[$fieldName] ?? NULL;
      $fieldBundles = $fieldInfo['bundles'];
      if ($fieldStorageDefinition) {
        // Check if the field class inherits from EntityReferenceItem, so we get
        // derived fields, too.
        $fieldType = $fieldInfo['type'];
        $fieldClass = $this->fieldTypePluginManager->getPluginClass($fieldType);
        $matchesEntityType = $fieldStorageDefinition->getSetting('target_type') === $ourEntityTypeId
          && is_a($fieldClass, EntityReferenceItem::class, TRUE)
          && !$fieldStorageDefinition->hasCustomStorage();
        if ($matchesEntityType) {
          $matchesBundles = FALSE;
          foreach ($fieldBundles as $fieldBundle) {
            $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($ourTargetType, $fieldBundle);
            $fieldDefinition = $fieldDefinitions[$fieldName] ?? NULL;
            if ($fieldDefinition) {
              $targetBundles = $fieldDefinition->getSetting('handler_settings')['target_bundles'] ?? FALSE;
              if (
                $targetBundles === NULL
                || is_array($targetBundles) && in_array($ourBundle, $targetBundles)
              ) {
                $matchesBundles = TRUE;
                break;
              }
            }
          }
          if ($matchesBundles) {
            $options[$fieldName] = $fieldName;
          }
        }
      }
    }
    return $options;
  }

  /**
   * @inheritDoc
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    // Hackily circumvent notices in parent method.
    $this->configuration['target_bundles'] = NULL;
    $query = parent::buildEntityQuery($match, $match_operator);
    unset($this->configuration['target_bundles']);

    // Add our conditions.
    $targetFieldName = $this->getConfiguration()['target_field_name'] ?? NULL;
    if ($targetFieldName) {
      // This is not set always, but it looks like in the cases we need it.
      // @see \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager::getSelectionHandler
      $entity = $this->getConfiguration()['entity'];
      $entityIds = $entity ? [$entity->id()] : [];
      $query->condition($targetFieldName, $entityIds);
    }
    return $query;
  }

}
