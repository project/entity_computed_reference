<?php

namespace Drupal\entity_computed_reference\Plugin\Field\FieldType;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity_computed_reference_all_selectable' field type.
 *
 * @FieldType(
 *   id = "entity_computed_reference",
 *   label = @Translation("Computed Reference"),
 *   description = @Translation("An computed field whose values are defined by a separate selection handler."),
 *   category = @Translation("Computed Reference"),
 *   list_class = "\Drupal\entity_computed_reference\Plugin\Field\FieldType\ComputedItemList",
 * )
 */
class ComputedItem extends EntityReferenceItem {

  /**
   * @inheritDoc
   */
  public static function defaultFieldSettings() {
    return [
      'value_handler' => 'default',
      'value_handler_settings' => [],
    ] + parent::defaultFieldSettings();
  }

  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);

    // This is an adapted copy of the parent.
    // We use the same form structure for value plugin as for selection plugin.
    $fieldDefinition = $form_state->getFormObject()->getEntity();
    assert($fieldDefinition instanceof FieldDefinitionInterface);

    // Ajax: Parent sets #process=::fieldSettingsAjaxProcess, which add ajax
    // processing to all #ajax=true elements in the element subtree, that
    // updates the whole settings form.
    // Also it adds #element_vaidate=::fieldSettingsFormValidate, which we
    // override.
    $form['value_handler'] = [
      '#type' => 'details',
      '#title' => t('Computed values'),
      '#description' => t('Select a reference plugin that narrows down the computed values of this field. Use default handler to not narrow down the selection. Sort and create-entity settings are ignored.'),
      '#open' => TRUE,
      '#tree' => TRUE,
      // This removes the outer 'value_handler' from #parents.
      '#process' => [[get_class($this), 'formProcessMergeParent']],
    ];

    $form['value_handler']['value_handler'] = [
      '#type' => 'select',
      '#title' => t('Computed values method'),
      '#options' => $this->getSelectionHandlerOptions(),
      '#default_value' => $fieldDefinition->getSetting('value_handler'),
      '#required' => TRUE,
      '#ajax' => TRUE,
      '#limit_validation_errors' => [],
    ];
    $form['value_handler']['handler_submit'] = [
      '#type' => 'submit',
      '#value' => t('Change handler'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      // Yes, we can copy this, it's just setRebuild().
      '#submit' => [[get_class($this), 'settingsAjaxSubmit']],
    ];

    $form['value_handler']['value_handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    // It's OK to use the returned handler, as it falls back to 'broken'.
    $valueHandler = static::getValueHandler($fieldDefinition);
    $form['value_handler']['value_handler_settings'] += $valueHandler->buildConfigurationForm([], $form_state);

    return $form;
  }

  public static function fieldSettingsFormValidate(array $form, FormStateInterface $form_state) {
    // #element_validate handler, @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::fieldSettingsForm
    parent::fieldSettingsFormValidate($form, $form_state);
    $fieldDefinition = $form_state->getFormObject()->getEntity();
    $valueHandler = static::getValueHandler($fieldDefinition);
    // $form is only the relevant settings form, as this in #element_validate.
    $valueHandler->validateConfigurationForm($form, $form_state);
  }

  public static function getSelectionHandler(FieldDefinitionInterface $field_definition, EntityInterface $entity = NULL): ?SelectionInterface {
    $selectionPluginManager = \Drupal::service('plugin.manager.entity_reference_selection');
    assert($selectionPluginManager instanceof SelectionPluginManagerInterface);
    return $selectionPluginManager->getSelectionHandler($field_definition, $entity);
  }

  public static function getValueHandler(FieldDefinitionInterface $field_definition, EntityInterface $entity = NULL): ?SelectionInterface {
    // Adapted from @see \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager::getSelectionHandler
    $options = $field_definition->getSetting('value_handler_settings') ?: [];
    $options += [
      'target_type' => $field_definition->getFieldStorageDefinition()->getSetting('target_type'),
      'entity' => $entity,
    ];
    $handler = $field_definition->getSetting('value_handler');
    if ($handler) {
      $options += [
        'handler' => $handler,
      ];
    }
    $selectionPluginManager = \Drupal::service('plugin.manager.entity_reference_selection');
    assert($selectionPluginManager instanceof SelectionPluginManagerInterface);
    $instance = $selectionPluginManager->getInstance($options);
    // It's OK to assert the returned handler, as it falls back to 'broken'.
    assert($instance);
    return $instance;
  }

  protected function getSelectionHandlerOptions(): array {
    // Factored out from @see \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem::fieldSettingsForm
    // Get all selection plugins for this entity type.
    $selection_plugins = \Drupal::service('plugin.manager.entity_reference_selection')
      ->getSelectionGroups($this->getSetting('target_type'));
    $handlers_options = [];
    foreach (array_keys($selection_plugins) as $selection_group_id) {
      // We only display base plugins (e.g. 'default', 'views', ...) and not
      // entity type specific plugins (e.g. 'default:node', 'default:user',
      // ...).
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':' . $this->getSetting('target_type'), $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':' . $this->getSetting('target_type');
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }
    return $handlers_options;
  }

}
