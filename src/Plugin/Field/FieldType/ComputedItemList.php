<?php

namespace Drupal\entity_computed_reference\Plugin\Field\FieldType;

use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\TypedData\ComputedItemListTrait;

class ComputedItemList extends EntityReferenceFieldItemList implements EntityReferenceFieldItemListInterface {

  use ComputedItemListTrait;

  protected function computeValue() {
    $selectionHandler = ComputedItem::getSelectionHandler($this->getFieldDefinition(), $this->getEntity());
    $valueHandler = ComputedItem::getValueHandler($this->getFieldDefinition(), $this->getEntity());
    // Start with selection handler to use its sort configuration.
    $ids = $this->getEntityIds($selectionHandler);
    $ids = $valueHandler->validateReferenceableEntities($ids);
    $this->setItemsFromIds($ids);
  }

  protected function getEntityIds(SelectionInterface $selection): array {
    $referenceable = $selection->getReferenceableEntities();
    $ids = [];
    foreach ($referenceable as $bundle => $labels) {
      foreach ($labels as $id => $label) {
        $ids[] = $id;
      }
    }
    return $ids;
  }

  protected function setItemsFromIds(array $ids): void {
    $ids = array_values($ids);
    $this->list = array_map(function ($i) use ($ids) {
      return $this->createItem($i, $ids[$i]);
    }, array_keys($ids));
  }

}
